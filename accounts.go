package gdax

import "log"

type Accounts struct {
	Endpoint
}

type Account struct {
	ID        string  `json:"id"`
	Currency  string  `json:"currency"`
	Balance   float64 `json:"balance,string"`
	Holds     float64 `json:"holds,string"`
	Available float64 `json:"available,string"`
}

// All returns all accounts for client credentials
func (a *Accounts) All() ([]Account, error) {
	var accounts []Account

	err := a.client.Get("/accounts", &accounts)
	if err != nil {
		log.Println(err)
	}

	return accounts, err
}

// Get returns the individual account represented by accountId
func (a *Accounts) Get(accountID string) (Account, error) {
	var account Account

	err := a.client.Get("/accounts/"+accountID, &account)
	if err != nil {
		log.Println(err)
	}

	return account, err
}
