package gdax

type Endpoint struct {
	client *Client
}

type EndpointInterface interface {
	register(*Client)
}

func (e *Endpoint) register(client *Client) *Endpoint {
	e.client = client

	return e
}
