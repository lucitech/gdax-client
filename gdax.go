package gdax

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

type Client struct {
	key        string
	passphrase string
	secret     string
	Accounts   *Accounts
	Orders     *Orders
}

type GDAXError struct {
	Message string
}

const (
	GDAXURL = "https://api.gdax.com"
)

type ClientInterface interface {
	Get(string)
}

func New(key string, passphrase string, secret string) (*Client, error) {
	client := &Client{key, passphrase, secret, &Accounts{}, &Orders{}}

	client.Accounts.register(client)
	client.Orders.register(client)

	return client, nil
}

func (c *Client) Get(endpoint string, returnInterface interface{}, params ...string) error {
	path := endpoint
	path += c.getParamString(params)

	request, err := http.NewRequest("GET", GDAXURL+path, nil)
	if err != nil {
		log.Println(err)
	}
	request = c.sign(request, "GET", path)

	response := c.doRequest(request)

	body, err := c.checkStatus(response)
	if err != nil {
		log.Println(err)
	}

	err = json.Unmarshal(body, returnInterface)
	if err != nil {
		log.Println(err)
	}

	return err
}

func (c *Client) Post(endpoint string, returnInterface interface{}, params ...map[string]string) error {
	path := endpoint
	jsonBody := c.getPostBody(params...)
	log.Println(string(jsonBody))
	br := bytes.NewReader(jsonBody)

	request, err := http.NewRequest("POST", GDAXURL+path, br)
	if err != nil {
		log.Println(err)
	}

	request = c.sign(request, "POST", path, string(jsonBody))

	response := c.doRequest(request)

	body, err := c.checkStatus(response)
	if err != nil {
		log.Println(err)
	}

	err = json.Unmarshal(body, returnInterface)
	if err != nil {
		log.Println(err)
	}

	return err
}

func (c *Client) getPostBody(params ...map[string]string) []byte {
	postMap := make(map[string]string)

	for _, paramSet := range params {
		for key, value := range paramSet {
			postMap[key] = value
		}
	}

	body, err := json.Marshal(postMap)
	if err != nil {
		log.Println(err)
	}

	return body
}

func (c *Client) getBody(response *http.Response) []byte {
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println(err)
	}
	return body
}

func (c *Client) checkStatus(response *http.Response) ([]byte, error) {
	var err error

	body := c.getBody(response)

	if response.StatusCode != 200 {
		var gdaxError GDAXError
		json.Unmarshal(body, &gdaxError)
		err = errors.New(gdaxError.Message)
	}
	return body, err
}

func (c *Client) sign(request *http.Request, verb string, path string, body ...string) *http.Request {
	timestamp := time.Now().Unix()

	signature := c.getSignature(timestamp, verb, path, body...)

	request.Header.Set("Accept", "application/json")
	request.Header.Set("Content-Type", "application/json")

	request.Header.Set("CB-ACCESS-KEY", c.key)
	request.Header.Set("CB-ACCESS-SIGN", signature)
	request.Header.Set("CB-ACCESS-TIMESTAMP", strconv.Itoa(int(timestamp)))
	request.Header.Set("CB-ACCESS-PASSPHRASE", c.passphrase)

	return request
}

func (c *Client) getParamString(params []string) string {
	paramString := ""

	key := ""
	for i, element := range params {
		if i%2 == 0 {
			key = element
		} else {
			if i > 1 {
				paramString += "&"
			}
			paramString = paramString + url.QueryEscape(key) + "=" + url.QueryEscape(element)
		}
	}

	if len(paramString) > 0 {
		paramString = "?" + paramString
	}

	return paramString
}

func (c *Client) getSignature(timestamp int64, method string, path string, body ...string) string {
	data := ""

	if len(body) > 0 {
		data = body[0]
	}

	composition := fmt.Sprintf("%s%s%s%s", strconv.Itoa(int(timestamp)), method, path, data)

	decodedSecret, err := base64.StdEncoding.DecodeString(c.secret)
	if err != nil {
		log.Println(err)
	}

	hash := hmac.New(sha256.New, decodedSecret)
	hash.Write([]byte(composition))

	return base64.StdEncoding.EncodeToString(hash.Sum(nil))
}

func (c *Client) doRequest(request *http.Request) *http.Response {
	client := &http.Client{
		Timeout: time.Second * 5,
	}

	response, err := client.Do(request)
	if err != nil {
		log.Println(err)
	}

	return response
}
