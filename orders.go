package gdax

import (
	"log"
	"strconv"
)

type Orders struct {
	Endpoint
}

type Order struct {
	ID            string  `json:"id"`
	Price         float64 `json:"price,string"`
	Size          float64 `json:"size,string"`
	ProductID     string  `json:"product_id"`
	Side          string  `json:"side"`
	STP           string  `json:"stp"`
	Type          string  `json:"type"`
	TimeInForce   string  `json:"time_in_force"`
	PostOnly      bool    `json:"post_only"`
	CreatedAt     string  `json:"created_at"`
	FillFees      float64 `json:"fill_fees,string"`
	FilledSize    float64 `json:"fill_size,string"`
	ExecutedValue float64 `json:"executed_value,string"`
	Status        string  `json:"status"`
	Settled       bool    `json:"settled"`
}

func (o *Orders) Buy(product string, currency string, amount float64) (Order, error) {
	parameters := map[string]string{
		"type":       "market",
		"side":       "buy",
		"product_id": product + "-" + currency,
		"funds":      strconv.FormatFloat(amount, 'f', -1, 64),
	}

	var order Order

	err := o.client.Post("/orders", &order, parameters)
	if err != nil {
		log.Println(err)
	}

	var orderList []Order
	cont := true
	for cont == true {
		cont = false
		o.client.Get("/orders", &orderList)
		for _, orderStatus := range orderList {
			if order.ID == orderStatus.ID {
				cont = true
			}
		}
	}

	return order, err
}

func (o *Orders) LimitBuy(product string, currency string, price float64, size float64) (Order, error) {
	parameters := map[string]string{
		"type":       "limit",
		"side":       "buy",
		"product_id": product + "-" + currency,
		"price":      strconv.FormatFloat(price, 'f', -1, 64),
		"size":       strconv.FormatFloat(size, 'f', -1, 64),
	}

	var order Order

	err := o.client.Post("/orders", &order, parameters)
	if err != nil {
		log.Println(err)
	}

	var orderList []Order
	cont := true
	for cont == true {
		cont = false
		o.client.Get("/orders", &orderList)
		for _, orderStatus := range orderList {
			if order.ID == orderStatus.ID {
				cont = true
			}
		}
	}

	return order, err
}

func (o *Orders) Sell(product string, currency string, amount float64) (Order, error) {
	parameters := map[string]string{
		"type":       "market",
		"side":       "sell",
		"product_id": product + "-" + currency,
		"size":       strconv.FormatFloat(amount, 'f', -1, 64),
	}

	var order Order

	err := o.client.Post("/orders", &order, parameters)
	if err != nil {
		log.Println(err)
	}

	var orderList []Order
	cont := true
	for cont == true {
		cont = false
		o.client.Get("/orders", &orderList)
		for _, orderStatus := range orderList {
			if order.ID == orderStatus.ID {
				cont = true
			}
		}
	}

	return order, err
}
